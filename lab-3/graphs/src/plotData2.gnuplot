## This script plots data points from a file

filename  = "graph1"
titlename = "some title"
yname     = "I / mA"
xname     = "V / V"


set title       titlename
set xlabel      xname
set ylabel      yname
set grid
set term pngcairo
set output "../../img/" . filename  . ".png"

#set xrange [0.001:0.005]
#set yrange [20:500]
set autoscale
# set logscale
# set logscale y


# set label "Yield point" at 0.003, 260

######################################################

## plot a best-fit line from data
title_f(a,b) = sprintf('f(x) = %.2fx + %.2f', a, b)

f(x) = a*x + b
g(x) = c*x + d
h(x) = e*x + f


fit f(x) "../data/data2.dat" using 1:2 via a, b
fit g(x) "../data/data3.dat" using 1:2 via c, d
fit h(x) "../data/data3.dat" using 1:2 via e, f

######################################################


plot \
    "../data/data2.dat" using 1:2 title "y" with lines, \
    "../data/data3.dat" using 1:2 title "y2" with points, \
    f(x) title title_f(a,b), \
    f(x) title title_f(e,f), \
    g(x) title title_f(c,d)

