## This script plots data points from a file

 filename  = "graph0"
titlename = "I_c VS V_ce curve"
yname     = "I_c / A"
xname     = "V_ce / V"


set title       titlename
set xlabel      xname
set ylabel      yname
set grid
set term pngcairo
set output "../../img/" . filename  . ".png"

set autoscale
set xrange [-100:5]
#set yrange [20:500]
# set logscale
# set logscale y


# set label "Yield point" at 0.003, 260

######################################################

## plot a best-fit line from data
title_f(a,b) = sprintf('f(x) = %.8fx + %.8f', a, b)

f(x) = a*x + b
g(x) = c*x + d
h(x) = e*x + f
k(x) = g*x + h


fit f(x) "../data/jhtong-lab3-0.dat" using 1:2 via a, b
fit g(x) "../data/jhtong-lab3-1.dat" using 1:2 via c, d
fit h(x) "../data/jhtong-lab3-2.dat" using 1:2 via e, f
fit k(x) "../data/jhtong-lab3-3.dat" using 1:2 via g, h
######################################################


m(x) = 0.00000188*x + 0.00138662
n(x) = 0.00001009*x + 0.00167783
o(x) = 0.00001831*x + 0.00197232
p(x) = 0.00002346*x + 0.00228636

plot \
    "../data/jhtong-lab3-0.dat" using 1:2 title "" with points, \
    "../data/jhtong-lab3-1.dat" using 1:2 title "" with points, \
    "../data/jhtong-lab3-2.dat" using 1:2 title "" with points, \
    "../data/jhtong-lab3-3.dat" using 1:2 title "" with points, \
    f(x) title "I_b = 5uA " . title_f(a,b), \
    g(x) title "I_b = 6uA " . title_f(c,d), \
    h(x) title "I_b = 7uA " . title_f(e,f), \
    k(x) title "I_b = 8uA " . title_f(g,h)

    #m(x) with lines title "m(x)"


    #"../data/data3.dat" using 1:2 title "y2" with points, \

