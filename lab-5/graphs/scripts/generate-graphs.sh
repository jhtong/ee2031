#!/bin/bash 

CWD=`pwd`
FILES=${CWD}/../src/*.gnuplot

for FILE in $FILES; do
    echo "Generating ${FILE}.png..\n"
    gnuplot $FILE
done

