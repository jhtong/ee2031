## This script plots data points from a file

 filename  = "graph0"
titlename = "Sallen-Key Active Low-Pass Filter Plot"
yname     = "Gain (dB)"
xname     = "Frequency f (Hz)"


set title       titlename
set xlabel      xname
set ylabel      yname
set grid
set term pngcairo
set output "../../img/" . filename  . ".png"

set autoscale
#set xrange [-100:5]
#set yrange [20:500]
# set logscale
set logscale x


# set label "Yield point" at 0.003, 260

plot \
    "../data/data.dat" using 1:2 title "" with lines, \
    "../data/data.dat" using 1:2 title "" with points
